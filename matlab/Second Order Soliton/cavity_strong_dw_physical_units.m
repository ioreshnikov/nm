% Simulation space.
mint = -40.0 * u.ps;
maxt = +40.0 * u.ps;
nt   = 2^14;

minz =  0.0 * u.m;
maxz = 48.0 * u.m;
nz   = 2^9;

t = linspace(mint, maxt, nt);
z = linspace(minz, maxz, nz);

% Fiber parameters.
beta2 = -14.2   * u.ps^2 / u.km;
beta3 =   0.087 * u.ps^3 / u.km;
betas = [beta2, beta3];
gamma = 2 / u.W / u.km;

% Soliton parameters.
l0 = 1470 * u.nm;
f0 = 2 * pi * u.c / l0;
t0 = 62.5 * u.fs;
t1 =  1.5 * u.ps;
p0 = 1817 * u.W;
cavity = 2 * sqrt(p0) * sech((t - t1)/t0) ...
       + 2 * sqrt(p0) * sech((t + t1)/t0);

% Dispersive wave.
t0 = 5 * t0;
l1 = 1130 * u.nm;
f1 = 2 * pi * u.c / l1;
p1 = p0 / 75;
dispwave = sqrt(p1) * sech(t/t0) ...
                   .* exp(-1j * (f1 - f0) * t);

input = cavity + dispwave;

% Integrate the equation.
[z, t, f, states, spectra] ...
    = solver.nlsehodla(z, t, input, betas, gamma);

% Save the results.
prefix = '2soliton_cavity_strong_dw_physical_units';
view.timedomain(z, t, states, [prefix '_td.dat'], 2);
view.freqdomain(z, f, spectra, [prefix '_fd.dat'], 2);
view.wldomain(z, f, f0, spectra, [prefix '_wl.dat'], 2);
view.wlspectrum(f, f0, spectra(1,   :), [prefix '_is.dat']);
view.wlspectrum(f, f0, spectra(end, :), [prefix '_os.dat']);
