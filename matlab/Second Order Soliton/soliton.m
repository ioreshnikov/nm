% Simulation space.
mint = -40.0 * u.ps;
maxt = +40.0 * u.ps;
nt   = 2^14;

minz = 0.0 * u.m;
maxz = 1.0 * u.m;
nz   = 2^8;

t = linspace(mint, maxt, nt);
z = linspace(minz, maxz, nz);

% Fiber parameters.
beta  = -14.2   * u.ps^2 / u.km;
gamma = 2 / u.W / u.km;

% Soliton parameters.
l0 = 1470 * u.nm;
f0 = 2 * pi * u.c / l0;
t0 = 62.5 * u.fs;
p0 = 1817 * u.W;
input = 2 * sqrt(p0) * sech(t/t0);

% Integrate the equation.
[z, t, f, states, spectra] ...
    = solver.nlse(z, t, input, beta, gamma);

% Save the results.
prefix = '2soliton_physical_units';
view.timedomain(z, t, states, [prefix '_td.dat'], 2);
view.freqdomain(z, f, spectra, [prefix '_fd.dat'], 2);
view.wldomain(z, f, f0, spectra, [prefix '_wl.dat'], 2);
view.wlspectrum(f, f0, spectra(1,   :), [prefix '_is.dat']);
view.wlspectrum(f, f0, spectra(end, :), [prefix '_os.dat']);
