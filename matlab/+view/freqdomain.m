function freqdomain(z, f, spectra, filename, subsampling)

if nargin == 5
    f = f(1 : subsampling : end - 1);
    spectra = spectra(:, 1 : subsampling : end - 1);
end

% Preparing the data being imaged.
im = abs(spectra);
im = im / max(max(im));

if nargin > 3 && ~isempty(filename)
    fprintf('Saving frequency domain data ... ');
    output = view.pack(f, z, im);
    dlmwrite(filename, output);
    fprintf('Done.\n');
    return
end

pcolor(f, z, 20 * log10(im));
shading flat;
caxis([-60, 0]);
colormap(gca, jet);
colorbar;

xlabel('Frequency');
ylabel('Distance');

end
