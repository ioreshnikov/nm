function wldomain(z, f, f0, spectra, filename, subsampling)

if nargin == 6
    f = f(1 : subsampling : end - 1);
    spectra = spectra(:, 1 : subsampling : end - 1);
end

l = 2 * pi * u.c ./ (f + f0);

if nargin >= 5 && ~isempty(filename)
    % Wavelength grid is nonuniform. Unfortunately there is no simple way
    % to plot a small image in gnuplot over nonuniform grids. The
    % best way is simply to resample the spectra.
    fprintf('Saving wavelength domain data ... ');
    l_ = linspace(l(1), l(end), length(l));
    im = interp2(l, z, spectra, l_, z);
    im = abs(im);
    im = im / max(max(im));
    output = view.pack(l_, z, im);
    dlmwrite(filename, output);
    fprintf('Done.\n');
    return
end

figure();
pcolor(l, z, abs(spectra));
shading flat;
colormap(jet);
colorbar();

end
