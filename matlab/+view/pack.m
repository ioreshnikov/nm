function m = pack(x, y, z)
nx = length(x);
ny = length(y);
m  = zeros(ny + 1, nx + 1);
m(1, 1) = nx;
m(1, 2:end) = x;
m(2:end, 1) = y;
m(2:end, 2:end) = z;
end
