function wlspectrum(f, f0, spectrum, filename)
l = 2 * pi * u.c ./ (f + f0);
s = abs(spectrum);
s = s / max(s);

if nargin == 4
    output = zeros(length(l), 2);
    output(:, 1) = l;
    output(:, 2) = s;
    dlmwrite(filename, output);
    return
end

figure();
plot(l / u.nm, s);
end