function timedomain(z, t, states, filename, subsampling)

if nargin == 5
    t = t(1 : subsampling : end - 1);
    states = states(:, 1 : subsampling : end - 1);
end

% Prepare the data being imaged.
im = abs(states);
im = im / max(max(im));

if nargin > 3 && ~isempty(filename)
    fprintf('Saving time domain data ... ');
    output = view.pack(t, z, im);
    dlmwrite(filename, output);
    fprintf('Done.\n');
    return
end

pcolor(t, z, 20 * log10(im));
shading flat;
caxis([-60, 0]);
colormap(gca, hot);
colorbar;

xlabel('Time');
ylabel('Distance');

end
