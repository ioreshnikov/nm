function spectrum(f, spectrum, filename)

if nargin == 3
    output = zeros(length(f), 2);
    output(:, 1) = f;
    output(:, 2) = abs(spectrum);
    dlmwrite(filename, output);
end

figure();
plot(f, abs(spectrum));
end
