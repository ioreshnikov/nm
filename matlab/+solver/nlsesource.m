function [z, t, f, states, spectra] = nlsesource(z, t,  input, ...
                                                 betas, gamma, ...
                                                 sa, st, sk)
% nlsesource integrates evolution of the initial distribution `u`
% according to nonlinear Schrodinger equation accounting for
% higher-order dispersion terms and external harmonic-in-z delta-in-t
% source which emulates ultra-long external dispersive waves.

% Find the parameters of the time and distange grids.
nt = length(t);
dt = t(2) - t(1);
tmin = min(t);
tmax = max(t);

nz = length(z);
zmax = max(z);

% Calculate the Fourier frequencies.
f = 2 * pi / (nt * dt) * [-nt/2 : nt/2 - 1]';

% Calculate the dispersion operator.
disp = 0;
for n = 1:length(betas)
    disp = disp + betas(n) / factorial(n + 1) * f.^(n + 1);
end
disp = fftshift(disp);

% Construct the linear absorber profile.
absorber = zeros(size(t));
width = (tmax - tmin) / 50;
absorber = absorber + sech((t - tmin) / width);
absorber = absorber + sech((t - tmax) / width);
absorber = 10 * absorber';

% Construct the source amplitude.
% source = sa * sech((t - st));
source = zeros(size(t));
[~, nearest] = min(abs(t - st));
source(nearest) = sa;
source = source';

% Right-hand's side of the equaiton.
function r = rhs(z, spectrum_)
    spectrum = exp(1j * disp * z) .* spectrum_;
    state  = fft(spectrum);
    nonlin = gamma * abs(state).^2 .* state;
    atten  = 1j * absorber .* state;
    force  = source * exp(1j * sk * z);
    r = 1j * exp(-1j * disp * z) ...
          .* ifft(nonlin + force + atten);
end

% Status report function.
function status = report(z, spectrum_, flag)
    status = 0;
    if isempty(flag)
        percentage = 100 * z / zmax;
        fprintf('\t%02.1f%% complete\n', percentage);
        return
    end
    if strcmp(flag, 'init')
        fprintf('Integrating ...\n');
        return
    end
    if strcmp(flag, 'done')
        fprintf('Done.\n');
        return
    end
end

% Configure the solver.
options = odeset('RelTol',      1E-5,  ...
                 'AbsTol',      1E-12, ...
                 'NormControl', 'on',  ...
                 'OutputFcn',   @report);

% Integrate.
state     = input;
spectrum  = ifft(state);
spectrum_ = spectrum;
[z, spectra_] = ode45(@rhs, z, spectrum_, options);

% Postprocess.
spectra = zeros(nz, nt);
states  = zeros(nz, nt);
for i = 1:nz
    spectra(i, :) = spectra_(i, :) .* exp(1j * disp' * z(i));
    states(i, :)  = fft(spectra(i, :));
    spectra(i, :) = fftshift(spectra(i, :)) ./ dt;
end

end
