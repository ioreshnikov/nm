% Simulation space
mint = -40.0 * u.ps;
maxt = +40.0 * u.ps;
nt   = 2^14;

minz =  0.0 * u.m;
maxz = 46.0 * u.m;
nz   = 2^8;

t = linspace(mint, maxt, nt);
z = linspace(minz, maxz, nz);

% Fiber parameters.
% Anomalous dispersion segment.
betam   = -5.16 * u.ps^2 / u.km;
lengthm = 24.0 * u.m;
% Normal dispersion segment.
betap   = +4.26 * u.ps^2 / u.km;
lengthp = 22.0 * u.m;
% Dispersion map.
betas = [ lengthm/2, betam ; ...
          lengthp  , betap ; ...
          lengthm/2, betam   ...
        ];
% Nonlinearity coefficient.
gamma = 1.72 / u.W / u.km;

% Soliton parameters.
l0 = 1540 * u.nm;
f0 = 2 * pi * u.c / l0;
t0 = 62.5 * u.fs;
p0 = abs(betam) / gamma / t0^2;

input = sqrt(p0) * exp(- (t/t0).^2 / 2);

[z, t, f, states, spectra] = ...
    solver.nlsedm(z, t, input, betas, gamma);
