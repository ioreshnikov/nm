function rcoeff(f, fmin, betas)
    reflection =  cosh(sqrt(15)/2 * pi)^2 ...
              ./ (cosh(sqrt(15)/2 * pi)^2 + sinh(pi * (f - fmin)).^2);

    figure();
    plot(f, reflection);
    xlabel('Frequency');
    ylabel('Reflection Coefficient');
end
