% Simulation space
mint = -400.0;
maxt = +400.0;
nt   =   2^14;

minz =   0.0;
maxz =  80.0;
nz   =   2^9;

t = linspace(mint, maxt, nt);
z = linspace(minz, maxz, nz);

% Fiber parameters.
beta2 = -1.0;
beta3 = -0.06;
betas = [beta2, beta3];
gamma = +1.0;

% Soliton cavity.
sa = 2.0;
input = sa * sech(sa * (t + sd/2)) + sa * sech(sa * (t - sd/2));

% Dispwave.
da = 0.100;
dw =  50.0;
dc = 100.0;
dp = 12.0;
envelope = exp(- (t - dc).^dp / dw^dp);
dispwave = da * envelope .* exp(-1j * df * t);

input = input + dispwave;

% Integrating the equation.
tic();
[z, t, f, states, spectra] = ...
    solver.nlsehodla(z, t, input, betas, gamma);
toc();

% Postprocessing.
% Filter the solitons and Cherenkov radiation.
dt = t(2) - t(1);
spectra_ = spectra;
spectra_(:, f > -20) = 0;
states_ = dt * fft(fftshift(spectra_, 2), [], 2);

% Find the coordinates of the solitons.
left  = zeros(size(z));
right = zeros(size(z));
for i = 1:length(z)
    state = states(i, :);
    % Find the first soliton position ...
    [~, first]  = max(state);
    % Clear everything in the vicinity of the soliton.
    vicinity = (t > t(first) - 2/sa) & (t < t(first) + 2/sa);
    state(vicinity) = 0;
    % Find the second soliton position.
    [~, second] = max(state);
    % Pick which one is the left and which one is the right.
    left(i)  = min(first, second);
    right(i) = max(first, second);
end

incident    = trapz(abs(dispwave).^2);
transmitted = zeros(size(z));
trapped     = zeros(size(z));
reflected   = zeros(size(z));

for i = 1:length(z)
    state = states_(i, :);
    l = left(i);
    r = right(i);
    % Find the between, the left and the right regions.
    b = (t > t(l)) & (t < t(r));
    l = t < t(l);
    r = t > t(r);
    transmitted(i) = trapz(abs(state(l)).^2) / incident;
    trapped(i)     = trapz(abs(state(b)).^2) / incident;
    reflected(i)   = trapz(abs(state(r)).^2) / incident;
end


% View the results.
prefix  = '../debug/Soliton Interferometer/';
postfix = '_df=%.3f_da=%.3f_sd=%.3f.png';

figure();
view.timedomain(z, t, abs(states).^0.25,  [], 2^5);
filename = sprintf([prefix 'timedomain' postfix], df, da, sd);
print(filename, '-dpng');
close();

figure();
view.freqdomain(z, f, abs(spectra).^0.25, [], 2^5);
filename = sprintf([prefix 'freqdomain' postfix], df, da, sd);
print(filename, '-dpng');
close();

figure();
plot(z, transmitted, ...
     z, trapped,     ...
     z, reflected);
legend('Transmitted', 'Trapped', 'Reflected');
xlabel('Distance');
filename = sprintf([prefix 'coeffs' postfix], df, da, sd);
print(filename, '-dpng');
close();

figure();
plot(z, t(left), z, t(right));
xlabel('Distance');
ylabel('Time');
filename = sprintf([prefix 'trajectory' postfix], df, da, sd);
print(filename, '-dpng');
close();

% Save the data files (wastes a lot of disk space)
prefix  = '../datafiles/Soliton Interferometer/';
postfix = '_df=%.3f_da=%.3f_sd=%.3f.dat';

filename = sprintf([prefix 'timedomain' postfix], df, da, sd);
view.timedomain(z, t, states, filename);

filename = sprintf([prefix 'freqdomain' postfix], df, da, sd);
view.freqdomain(z, f, spectra, filename);
