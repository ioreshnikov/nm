sd_ = 12.70 : 0.05 : 14.00;
transmitted_ = zeros(size(sd_));
trapped_     = zeros(size(sd_));
reflected_   = zeros(size(sd_));
output_      = zeros(length(sd_), 4);

for j = 1:length(sd_)
    % Run the simulation.
    sd = sd_(j);
    distance_steering_worker;

    % Collect the coefficients.
    % transmitted_(j) = transmitted(end);
    % trapped_(j)     = max(trapped);
    % reflected_(j)   = reflected(end);

    % Prepare output matrix.
    % output_(:, 1) = sd_;
    % output_(:, 2) = transmitted_;
    % output_(:, 3) = trapped_;
    % output_(:, 4) = reflected_;

    % figure(5);
    % plot(sd_, transmitted_, sd_, trapped_, sd_, reflected_);
    % xlabel('Soliton distance');
    % legend('Transmitted', 'Max. trapped', 'Reflected');

    % filename = [prefix 'coefficients.dat'];
    % dlmwrite(filename, output_);
    % drawnow();
end
