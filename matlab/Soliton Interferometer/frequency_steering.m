df_ = fliplr(-39.0 : 0.5 : -35.0);

for j = 1:length(df_)
    % Run the simulation.
    df = df_(j);
    frequency_steering_worker;
    % No post-processing is involved in this particular search, since we
    % are looking for nonlinear effects and interesting
    % configurations.
end
