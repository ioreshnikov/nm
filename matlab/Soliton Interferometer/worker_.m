% Simulation space
mint = -100.0;
maxt = +100.0;
nt   =   2^12;

minz =  0.0;
maxz = 10.0;
nz   =  2^9;

t = linspace(mint, maxt, nt);
z = linspace(minz, maxz, nz);

% Fiber parameters.
beta2 = -1.0;
beta3 = -0.06;
betas = [beta2, beta3];
gamma = +1.0;

% Soliton cavity.
sold = 7.1;
sola = 2.0;
input = sola * sech(sola * (t + sold/2)) ...
      + sola * sech(sola * (t - sold/2));
input = zeros(size(t));

% Source parameters.
srca = 0.005;
srct = 50.0;
srcf = -37.0;
srck = 0;
for n = 1 : length(betas)
    srck = srck + betas(n) / factorial(n + 1) * srcf.^(n + 1);
end

% Integrate the equation.
tic()
[z, t, f, states, spectra] = ...
    solver.nlsesource(z, t, input, betas, gamma, ...
                      srca, srct, srck);
toc()

figure();
view.timedomain(z, t, states, [], 2^4);

figure();
view.freqdomain(z, f, spectra, [], 2^4);
