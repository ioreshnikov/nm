% Initialization
% ==============

% Simulation space
mint = -200.0;
maxt = +800.0;
nt   =   2^14;

minz =  0.0;
maxz =  1.0; % was 240
nz   =  2^9;

t = linspace(mint, maxt, nt);
z = linspace(minz, maxz, nz);

% Fiber parameters.
beta2 = -1.0;
beta3 = -0.06;
betas = [beta2, beta3];
gamma = +1.0;

% Soliton cavity.
sd = 7.1;
sa = 2.0;
input = sa * sech(sa * (t + sd/2)) + sa * sech(sa * (t - sd/2));

% Dispwave.
da = 0.020;
dw = 250.0;
dc = 400.0;
df = -37.0;
dp = 12.0;
envelope = exp(- (t - dc).^dp / dw^dp);
dispwave = da * envelope .* exp(-1j * df * t);

input = input + dispwave;

% Integrating the equation.
tic();
[z, t, f, states, spectra] = ...
    solver.nlsehodla(z, t, input, betas, gamma);
toc();


% Saving the raw data
% =================

% It's better to save the full pictures of time and frequency domain
% first, free some memory and then begin the analysis.
prefix  = '../debug/Soliton Interferometer/';
postfix = '_df=%.3f_da=%.3f_sd=%.3f.png';

figure();
view.timedomain(z, t, abs(states),  [], 2^4);
filename = sprintf([prefix 'timedomain' postfix], df, da, sd);
print(filename, '-dpng');
close();

figure();
view.freqdomain(z, f, abs(spectra), [], 2^4);
filename = sprintf([prefix 'freqdomain' postfix], df, da, sd);
print(filename, '-dpng');
close();

prefix  = '../datafiles/Soliton Interferometer/';
postfix = '_df=%.3f_da=%.3f_sd=%.3f.dat';

filename = sprintf([prefix 'timedomain' postfix], df, da, sd);
view.timedomain(z, t, states, filename);

filename = sprintf([prefix 'freqdomain' postfix], df, da, sd);
view.freqdomain(z, f, spectra, filename);

% We don't need time domain data anymore.
clear 'states';


% Postprocessing
% ==============

% Separate the radiation and the solitons.
dt = t(2) - t(1);

radiation = spectra;
radiation(:, f > -20) = 0;
radiation(:, f < -50) = 0;
radiation = dt * fft(fftshift(radiation, 2), [], 2);

solitons = spectra;
solitons(:, f < -20) = 0;
solitons(:, f > +20) = 0;
solitons = dt * fft(fftshift(solitons, 2), [], 2);

% We can free spectral data now.
clear 'spectra';

% Find the coordinates of the solitons.
left  = zeros(size(z));
right = zeros(size(z));
leftamp  = zeros(size(z));
rightamp = zeros(size(z));

for i = 1:length(z)
    state = abs(solitons(i, :));
    % Find the first soliton position ...
    [firstamp, first]  = max(state);
    % Clear everything in the vicinity of the soliton.
    vicinity = (t > t(first) - 2/sa) & (t < t(first) + 2/sa);
    state(vicinity) = 0;
    % Find the second soliton position.
    [secondamp, second] = max(state);
    if first < second
        left(i)  = t(first);
        right(i) = t(second);
        leftamp(i)  = firstamp;
        rightamp(i) = secondamp;
    else
        left(i)  = t(second);
        right(i) = t(first);
        leftamp(i)  = secondamp;
        rightamp(i) = firstamp;
    end
end

% Separate the radiation into incident, transmitted, trapped and
% reflected components.
incident    = trapz(abs(dispwave).^2);
transmitted = zeros(size(z));
trapped     = zeros(size(z));
reflected   = zeros(size(z));

for i = 1:length(z)
    state = radiation(i, :);
    l = left(i);
    r = right(i);
    % Find the between, the left and the right regions.
    b = (t > l) & (t < tr);
    l = t < l;
    r = t > r;
    transmitted(i) = trapz(abs(state(l)).^2) / incident;
    trapped(i)     = trapz(abs(state(b)).^2) / incident;
    reflected(i)   = trapz(abs(state(r)).^2) / incident;
end

figure();
plot(z, transmitted, ...
     z, trapped,     ...
     z, reflected);
legend('Transmitted', 'Trapped', 'Reflected');
xlabel('Distance');
filename = sprintf([prefix 'coeffs' postfix], df, da, sd);
print(filename, '-dpng');
close();

figure();
plot(z, t(left), z, t(right));
xlabel('Distance');
ylabel('Time');
filename = sprintf([prefix 'trajectory' postfix], df, da, sd);
print(filename, '-dpng');
close();
