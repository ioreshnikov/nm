function disprelation(f, betas, f0)

function k = disp(f)
    k = 0;
    for n = 1:length(betas)
        k = k + betas(n) / factorial(n + 1) * f.^(n + 1);
    end
end

if nargin == 3
    reference = disp(f0) * ones(size(f));
else
    reference = zeros(size(f));
end

figure();
plot(f, disp(f), f, reference);
xlabel('Frequency');
ylabel('Wavenumber');

end