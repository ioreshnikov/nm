% Simulation space.
mint = -80.0;
maxt = +80.0;
nt   = 2^12;

minz = 0.0;
maxz = 1.0;
nz   = 2^8;

t = linspace(mint, maxt, nt);
z = linspace(minz, maxz, nz);

% Fiber parameters.
beta  = +1;
betas = [beta, 0];
gamma = +1;

% Soliton parameters.
bg = 1.0;
input = bg * tanh(bg * t);

% Integrate the equation.
[z, t, f, states, spectra] ...
    = solver.nlsehodnla(z, t, input, betas, gamma, bg);

% Save the results.
prefix = 'dark_soliton';
view.timedomain(z, t, states,  [prefix '_td.dat'], 2);
view.freqdomain(z, f, spectra, [prefix '_fd.dat'], 2);
