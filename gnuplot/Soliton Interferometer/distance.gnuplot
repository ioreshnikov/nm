#!/usr/bin/env gnuplot

# Input files.
input  = 'datafiles/Soliton Interferometer/distance_df=-37.000_da=0.050_sd=%.3f.dat';
output = 'output.tex';

set datafile separator ',';


# Terminal.
set terminal epslatex \
    standalone color  \
    size 8.8cm, 5.5cm \
    header '\usepackage[utf8]{inputenc}' \
    font '' 9;
set output output;


zmin =   0.0;
zmax = 140.0;


# Common plot settings.
set   multiplot;
set   tics front;
unset key;
unset colorbox;


# The one and only plot. I do use multiplot, but only to fix axis
# labels sticking out.
set origin 0.00, 0.00;
set size   1.00, 0.99;

set xrange [zmin : zmax];
set xlabel '$z$';

set ylabel 'Relative soliton delay' offset 0.5, 0.0;

set palette defined (0 '#ff0000', 1 '#0000ff');

plot for [i = 800 : 1400 : 5] sprintf(input, i / 100.0) \
                              using 1:2                 \
                              smooth acsplines          \
                              with lines                \
                              lc rgb 'gray';
