#!/usr/bin/env gnuplot

# Input files.
prefix  = 'datafiles/Soliton Interferometer/Distance Steering with a Short DW/';
postfix = '_df=-35.000_da=0.100_sd=8.000.dat';


timedomain = prefix . 'timedomain' . postfix;
freqdomain = prefix . 'freqdomain' . postfix;
output     = 'output.tex';

set datafile separator ',';


# Terminal.
set terminal epslatex \
    standalone color  \
    size 8.8cm, 5.5cm \
    header '\usepackage[utf8]{inputenc}' \
    font '' 9;
set output output;


# Display area.
tmin = -50;
tmax = +100;

fmin = -50;
fmax = +25;

zmin =   0.0;
zmax = 100.0;

dbmin = -60;
dbmax =   0;


# Common plot settings.
set   multiplot;
set   tics out;
unset key;
unset colorbox;


# Time domain plot.
set origin 0.00, 0.00;
set size   0.53, 0.99;

set xrange [tmin : tmax];
set xtics  50;
set xlabel '$t$';

set yrange [zmin : zmax];
set ylabel '$z$' offset 0.5, 0.0;

set cbrange [dbmin : dbmax];
set palette rgb 21, 22, 23;

set label 1 '\textcolor{white}{(a)}' \
    at graph 0.12, 0.08 center front;
plot timedomain nonuniform matrix \
     using   1:2:(20 * log10($3)) \
     every   1:1                  \
     with    image;


# Frequency domain plot.
set origin 0.50, 0.00;
set size   0.53, 0.99;

set xrange [fmin : fmax];
set xtics  25;
set xlabel '$\delta \omega$';

set yrange [zmin : zmax];
set ylabel '$z$' offset 0.5, 0.0;

set cbrange [dbmin : dbmax];
set palette defined ( 0 '#000090', \
                      1 '#000fff', \
                      2 '#0090ff', \
                      3 '#0fffee', \
                      4 '#90ff70', \
                      5 '#ffee00', \
                      6 '#ff7000', \
                      7 '#ee0000', \
                      8 '#7f0000' );

set label 1 '\textcolor{white}{(b)}' \
    at graph 0.88, 0.08 center front;
plot freqdomain nonuniform matrix \
     using   1:2:(20 * log10($3)) \
     every   1:1                  \
     with    image;


# Done.
unset multiplot;
