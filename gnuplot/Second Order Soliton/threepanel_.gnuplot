#!/usr/bin/env gnuplot


prefix  = '2soliton_cavity_weak_dw_physical_units';
tdinput = 'datafiles/' . prefix . '_td.dat';
wlinput = 'datafiles/' . prefix . '_wl.dat';
isinput = 'datafiles/' . prefix . '_is.dat';
osinput = 'datafiles/' . prefix . '_os.dat';
output  = prefix . '_threepanel.tex';

set datafile separator ',';

set terminal epslatex \
    standalone color  \
    size 8.8cm, 8.0cm \
    header '\usepackage{mathpazo}' \
    font '' 9;
set output output;


mint = -1.0;
maxt = +1.0;

minl = 1000.0;
maxl = 1600.0;

minz = 0.0;
maxz = 8.0;

nm = 1E-9;
ps = 1E-12;


set multiplot;
unset key;
unset colorbox;
set tics out;


# Time domain plot.
set origin 0.00, 0.30;
set size   0.53, 0.69;

set xrange [mint : maxt];
set xlabel 'Delay, ps';

set yrange [minz : maxz];
set ylabel 'Distance, m' offset 1.0, 0.0;

set palette rgbformulae 30, 31, 32;

set label 1 '\textcolor{white}{(a)}' \
    at graph 0.1, 0.95 center front;
plot tdinput nonuniform matrix      \
     using ($1 / ps):2:(sqrt($3)) \
     with image;


# Wavelength domain plot.
set origin 0.50, 0.30;
set size   0.53, 0.69;

set xrange [minl : maxl];
set xtics  200;
set xlabel 'Wavelength, nm';

set yrange [minz : maxz];
set ylabel 'Distance, m' offset 1.0, 0.0;

set label 1 '\textcolor{white}{(b)}' \
    at graph 0.1, 0.05 center front;
plot wlinput nonuniform matrix    \
     using ($1 / nm):2:(sqrt($3)) \
     every 4:1                    \
     with image;


# Input and ouput spectrum.
set origin 0.00, 0.00;
set size   1.00, 0.30;

set xrange [minl : maxl];
set xtics  100;
set xlabel 'Wavelength, nm';

unset yrange;
unset ylabel;
unset ytics;

set key horizontal top left;

set label 1 '(c)' at graph 0.97, 0.80 center front;
plot isinput using ($1 / nm):($2 ** 0.50) \
       with lines title 'input'  lw 1.2,  \
     osinput using ($1 / nm):($2 ** 0.50) \
       with lines title 'output' lw 1.2;


unset multiplot;
