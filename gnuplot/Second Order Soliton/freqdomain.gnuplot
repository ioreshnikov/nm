#!/usr/bin/env gnuplot


prefix = '2soliton_scattering_strong_dw_physical_units';
input  = 'datafiles/' . prefix . '_fd.dat';
output = prefix . '_fd.tex';

set datafile separator ',';


set terminal epslatex \
    standalone color  \
    size 8.8cm, 4.4cm \
    header '\usepackage{mathpazo}' \
    font '' 9;
set output output;


minf = -10.0;
maxf = +10.0;

minz = 0.0;
maxz = 4.0;


unset key;
unset colorbox;

# set xrange [minf : maxf];
set xlabel 'Frequency';

set yrange [minz : maxz];
set ylabel 'Distance, m';


plot input nonuniform matrix \
     using 1:2:(sqrt($3))    \
     with image;
