#!/usr/bin/env gnuplot


prefix = '2soliton_scvsinc_physical_units';
input  = 'datafiles/' . prefix . '.dat';
output = prefix . '.tex';


minl = 1000.0;
maxl = 1600.0;

km = 1E3;
nm = 1E-9;
ps = 1E-12;
fs = 1E-15;

beta2 = -14.200 * ps**2 / km;
beta3 =   0.087 * ps**3 / km;
c = 299792458;

sol = 1470;
delta(x) = 2*pi*c * (1.0 / (x * nm) - 1.0 / (sol * nm));
lhs(x) = 1.0/2 * beta2 * delta(x)**2 \
       + 1.0/6 * beta3 * delta(x)**3;


set view map;
unset surface;

set contour;
set cntrparam level discrete 0;

set isosamples 2000, 2000;

set xrange [1080 : 1180];
set xlabel '$\lambda_{inc}$, нм';

set yrange [1150 : 1350];
set ylabel '$\lambda_{sc}$, нм';

# Dumping the contour into temporary file.
tempfile = '/tmp/contour.dat';

set table tempfile;
splot (lhs(x) - lhs(y)) / (x - y);
unset table;


# Plotting the data into TeX file.
set terminal epslatex \
    standalone color  \
    size 8.8cm, 5.5cm \
    header '\usepackage[utf8]{inputenc}\usepackage[T2A]{fontenc}' \
    font '' 9;
set output output;


plot tempfile using 1:2 with lines       title 'теория', \
     input    using 1:2 with points pt 5 title 'модель';
