#!/usr/bin/env gnuplot


prefix = '2soliton_scattering_strong_dw_physical_units';
input  = 'datafiles/' . prefix . '_wl.dat';
output = prefix . '_wl.tex';

set datafile separator ',';


set terminal epslatex \
    standalone color  \
    size 8.8cm, 4.4cm \
    header '\usepackage{mathpazo}' \
    font '' 9;
set output output;


minl = 1000;
maxl = 1600;

minz = 0.0;
maxz = 4.0;


unset key;
# unset colorbox;

set xrange [minl : maxl];
set xlabel 'Wavelength, nm';

set yrange [minz : maxz];
set ylabel 'Distance, m';

set palette @jet;

plot input nonuniform matrix        \
     using ($1 / nm):2:($3 ** 0.25) \
     with image;
