#!/usr/bin/env gnuplot


prefix  = '2soliton_cavity_weak_dw_physical_units';
tdinput = 'datafiles/' . prefix . '_td.dat';
wlinput = 'datafiles/' . prefix . '_wl.dat';
output  = prefix . '_twopanel.tex';

set datafile separator ',';

set terminal epslatex \
    standalone color  \
    size 8.8cm, 5.5cm \
    header '\usepackage[utf8]{inputenc}\usepackage[T2A]{fontenc}' \
    font '' 9;
set output output;


mint = -0.5;
maxt = +0.5;

minl = 1070.0;
maxl = 1870.0;

minz = 0.0;
maxz = 1.0;

nm = 1E-9;
ps = 1E-12;


set multiplot;
unset key;
unset colorbox;
set tics out;


# Time domain plot
set origin 0.00, 0.00;
set size   0.53, 0.99;

set xrange [mint : maxt];
set xtics  0.25;
set xlabel '$t$, пc';

set yrange [minz : maxz];
set ylabel '$z$, м' offset 1.0, 0.0;

set label 1 '\textcolor{white}{(a)}' \
    at graph 0.9, 0.1 center front;
plot tdinput nonuniform matrix    \
     using ($1 / ps):2:(sqrt($3)) \
     every 1:1                    \
     with image;


# Wavelength domain plot.
set origin 0.50, 0.00;
set size   0.53, 0.99;

set xrange [minl : maxl];
set xtics  200;
set xlabel '$\lambda$, нм';

set yrange [minz : maxz];
set ylabel '$z$, м' offset 1.0, 0.0;

set palette rgb 33, 13, 10;

set label 1 '\textcolor{white}{(b)}' \
    at graph 0.1, 0.1 center front;
plot wlinput nonuniform matrix    \
     using ($1 / nm):2:(sqrt($3)) \
     every 4:1                    \
     with image;


unset multiplot;
