#!/usr/bin/env gnuplot


prefix  = '2soliton_scattering_wide_dw_physical_units';
tdinput = 'datafiles/' . prefix . '_td.dat';
wlinput = 'datafiles/' . prefix . '_wl.dat';
osinput = 'datafiles/' . prefix . '_os.dat';
output  = prefix . '_fourpanel.tex';

set datafile separator ',';

set terminal epslatex \
    standalone color  \
    size 8.8cm, 8.8cm \
    header '\usepackage[utf8]{inputenc}\usepackage[T2A]{fontenc}' \
    font '' 9;
set output output;


mint = -8.0;
maxt = +4.0;

minl = 1000.0;
maxl = 1300.0;

minl2 = 1075.0;
maxl2 = 1300.0;

minz =  0.0;
maxz = 12.0;

mink = -260;
maxk = -160;

nm = 1E-9;
ps = 1E-12;


set multiplot;
unset key;
unset colorbox;
set tics out;


set origin 0.00, 0.50;
set size   0.53, 0.49;

set xrange [mint : maxt];
set xtics  4;
set xlabel '$t$, пс' offset 0.0, 0.5;

set yrange [minz : maxz];
set ylabel '$z$, м' offset 1.0, 0.0;


set label 1 '\textcolor{white}{(a)}' \
    at graph 0.9, 0.1 center front;
plot tdinput nonuniform matrix    \
     using ($1 / ps):2:(sqrt($3)) \
     with image;


set origin 0.50, 0.50;
set size   0.53, 0.49;

set xrange [minl : maxl];
set xtics  100;
set xlabel '$\lambda$, нм';

set yrange [minz : maxz];
set ylabel '$z$, м' offset 1.0, 0.0;

set palette rgbformulae 33, 13, 10;

set label 1 '\textcolor{white}{(b)}' \
    at graph 0.1, 0.1 center front;
plot wlinput nonuniform matrix    \
     using ($1 / nm):2:(sqrt($3)) \
     with image;


set origin 0.00, 0.25;
set size   1.00, 0.25;

set xrange [minl2 : maxl2];
set xtics  50;
set xlabel '$\lambda$, нм';

set format x2 '';
set x2tics (1108, 1114, 1121, 1130, 1142, \
            1206, 1223, 1237, 1249, 1262);
set grid noxtics x2tics;
set xtics nomirror;

unset yrange;
unset ytics;
unset ylabel;

set label 1 '(c)' at graph 0.96, 0.70 center front;
plot osinput using ($1 / nm):(sqrt($2)) with lines;


# Final part. We need to plot analytical analytical resonance
# conditions.
set origin 0.00, 0.00;
set size   1.00, 0.25;

fs = 1E-15;
km = 1E3;

c  = 299792458;
l0 = 1470;
li = 1130;
t0 = 62.5 * fs;

beta2 = -14.200 * ps**2 / km;
beta3 =   0.087 * ps**3 / km;

z0 = pi/2 * t0**2 / abs(beta2);

delta(l)  = 2*pi*c * (1/(l * nm) - 1/(l0 * nm));
disp0(l)  = 1.0/2 * beta2 * delta(l)**2 + 1.0/6 * beta3 * delta(l)**3;
disp_1(l) = disp0(l) - 2*pi/z0;
disp1(l)  = disp0(l) + 2*pi/z0;
disp2(l)  = disp0(l) + 4*pi/z0;
disp3(l)  = disp0(l) + 6*pi/z0;

set xrange [minl2 : maxl2];
set xtics  50;
set xlabel '$\lambda$, нм';

set format x2 '';
set x2tics (1108, 1114, 1121, 1130, 1142, \
            1206, 1223, 1237, 1249, 1262);
set grid noxtics x2tics;
set xtics nomirror;

set yrange [mink : maxk];

set label 1 '(d)' at graph 0.96, 0.70 center front;
plot disp0(x)   with lines, \
     disp_1(li) with lines, \
     disp0(li)  with lines, \
     disp1(li)  with lines, \
     disp2(li)  with lines, \
     disp3(li)  with lines;


unset multiplot;
