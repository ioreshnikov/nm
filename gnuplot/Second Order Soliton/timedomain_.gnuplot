#!/usr/bin/env gnuplot


prefix = 'dark_soliton';
input  = 'datafiles/' . prefix . '_td.dat';
output = prefix . '_td.tex';

set datafile separator ',';


set terminal epslatex \
    standalone color  \
    size 8.8cm, 5.5cm \
    header '\usepackage[utf8]{inputenc}\usepackage[T2A]{fontenc}' \
    font '' 9;
set output output;


mint = -10.0;
maxt = +10.0;

minz = 0.0;
maxz = 1.0;


unset key;
unset colorbox;
set tics out;

set multiplot;

set origin 0.00, 0.00;
set size   1.00, 0.99;

set xrange [mint : maxt];
set xlabel '$\tau$';

set yrange [minz : maxz];
set ylabel '$\zeta$';


plot input nonuniform matrix \
     using 1:2:3             \
     with image;

unset multiplot;