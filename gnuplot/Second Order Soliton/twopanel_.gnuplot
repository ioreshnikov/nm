#!/usr/bin/env gnuplot


prefix  = 'dark_soliton';
tdinput = 'datafiles/' . prefix . '_td.dat';
fdinput = 'datafiles/' . prefix . '_fd.dat';
output  = prefix . '_twopanel.tex';

set datafile separator ',';

set terminal epslatex \
    standalone color  \
    size 8.8cm, 5.5cm \
    header '\usepackage[utf8]{inputenc}\usepackage[T2A]{fontenc}' \
    font '' 9;
set output output;


mint = -10.0;
maxt = +10.0;

minl = -10.0;
maxl = +10.0;

minz = 0.0;
maxz = 1.0;


set multiplot;
unset key;
unset colorbox;
set tics out;


# Time domain plot
set origin 0.00, 0.00;
set size   0.53, 0.99;

set xrange [mint : maxt];
set xtics  5.0;
set xlabel '$\tau$';

set yrange [minz : maxz];
set ylabel '$z$' offset 1.0, 0.0;

set label 1 '(a)' \
    at graph 0.9, 0.1 center front;
plot tdinput nonuniform matrix    \
     using 1:2:3 \
     every 1:1   \
     with image;


# Frequency domain plot.
set origin 0.50, 0.00;
set size   0.53, 0.99;

set xrange [minl : maxl];
set xtics  5.0;
set xlabel '$\omega$';

set yrange [minz : maxz];
set ylabel '$z$' offset 1.0, 0.0;

set palette rgb 33, 13, 10;

set label 1 '\textcolor{white}{(b)}' \
    at graph 0.1, 0.1 center front;
plot fdinput nonuniform matrix    \
     using 1:2:3 \
     every 4:1   \
     with image;


unset multiplot;
