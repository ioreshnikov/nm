#!/usr/bin/env gnuplot


prefix = '2soliton_scattering_strong_dw_physical_units';
input  = 'datafiles/' . prefix . '_td.dat';
output = prefix . '_td.tex';

set datafile separator ',';


set terminal epslatex \
    standalone color  \
    size 8.8cm, 4.4cm \
    header '\usepackage{mathpazo}' \
    font '' 9;
set output output;


mint = -1.0;
maxt = +1.0;

minz = 0.0;
maxz = 8.0;


unset key;
unset colorbox;

set xrange [mint : maxt];
set xlabel 'Delay, ps';

set yrange [minz : maxz];
set ylabel 'Distance, m';


plot input nonuniform matrix        \
     using ($1 * 1E12):2:(sqrt($3)) \
     with image;
